/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CONVERTER_H
#define CONVERTER_H

#include <string>
#include <sstream>

class Converter {
public:
    virtual ~Converter()=0;

    template <typename T>
    static std::string ToString(const T& i)
    {
        std::ostringstream stream;
        stream << i;
        return stream.str();
    }
    template <typename T>
    static T FromString(const std::string& str)
    {
        std::istringstream stream(str);
        T tmp;
        stream >> tmp;

        return tmp;
    }
};

#endif // CONVERTER_H
