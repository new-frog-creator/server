#include "Node.h"

Node::Node(const std::string& value) {
    m_Value = value;
}

Node::~Node() {
    std::map<std::string,Node*>::iterator it = m_Keys.begin();
    while(it != m_Keys.end()) {
	delete (*it).second;
	it++;
    }
}

Node& 
Node::Get(const std::string& key) {
    if(m_Keys.find(key) == m_Keys.end())
	m_Keys[key] = new Node;
    return *m_Keys.at(key);
}

void
Node::Set(const std::string& key,const std::string& value) {
    Get(key) = value;
}

const std::string& 
Node::Value() const {
    return m_Value;
}

Node&
Node::operator=(const std::string& value) {
    m_Value = value;
    return *this;
}

const std::map<std::string,Node*>&
Node::GetKeys() const {
    return m_Keys;
}

/*
std::ostream&
operator<<(std::ostream& stream,Node& node) {
    stream << node.Value;
    return stream;
}
// */
