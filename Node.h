#ifndef NODE_H
#define NODE_H

#include <string>
#include <map>
#include <ostream>

class PlayerManager;
class Node {
public:
	    Node(const std::string& value="");
	    ~Node();
    Node&   Get(const std::string& key);
    void    Set(const std::string& name,const std::string& value);
    const   std::string& Value() const;

    Node&   operator=(const std::string& value);

    const std::map<std::string,Node*>&
	    GetKeys() const; // WORK-AROUND FOR SAVING, TODO: IMPLEMENT VISITORS    


private:
    std::string m_Value;
    std::map <std::string,Node*> m_Keys;
};

//std::ostream& operator<<(std::ostream& steam, Node& node);
 
#endif
