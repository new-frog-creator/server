/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Player.h"

Player::Player()
{
    m_ID = 0;
}
Player::~Player()
{
    std::map<std::string,Node*>::iterator it = m_Keys.begin();
    while(it != m_Keys.end()) {
	delete (*it).second;
	it++;
    }
}
long unsigned int Player::GetID() const
{
    return m_ID;
}

const std::string& Player::GetName() const
{
    return m_Name;
}


void Player::SetID(const long unsigned int& id)
{
    m_ID = id;
}

void Player::SetName(const std::string& name)
{
    m_Name = name;
}

Node&
Player::Get(const std::string& name) {
    if(m_Keys.find(name) == m_Keys.end())
	m_Keys[name]= new Node();
    return *m_Keys[name];
}

void
Player::Set(const std::string& name, const std::string& value) {
    Get(name) = value;
}

const std::map<std::string,Node*>&
Player::GetKeys() const {
    return m_Keys;
}
