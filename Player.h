/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef PLAYER_H
#define PLAYER_H
#include <map>
#include <string>
#include "Node.h"

class PlayerManager;

class Player
{
public:
    Player();
    ~Player();

    unsigned long int 
	    GetID() const;
    const   std::string& GetName() const;

    void    SetID(const unsigned long int& id);
    void    SetName(const std::string& name);

    Node&   Get(const std::string& name);
    void    Set(const std::string& name, const std::string& value);
    const std::map<std::string,Node*>&
	    GetKeys() const;// WORK-AROUND TODO: IMPLEMENT VISITORS
protected:


private:
    std::map<std::string,Node*> m_Keys;
    std::string m_Name;
    unsigned long int m_ID;
};

#endif // PLAYER_H
