/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef PLAYERMANAGER_H
#define PLAYERMANAGER_H
#include <string>

#include "Player.h"

class PlayerManager {

public:
    virtual ~PlayerManager(){};

    virtual Player& 
	    ByID(unsigned long int id)=0;

    virtual Player& 
	    ByName(const std::string& name)=0;

    virtual Player&
	    Create(const std::string& name)=0;

    virtual void 
	    Save(Player& player)=0;

    virtual void 
	    Delete(Player& player)=0;

    virtual bool 
	    HasID(unsigned long int id)=0;

    virtual bool 
	    HasName(const std::string& name)=0;
};

#endif // PLAYERMANAGER_H
