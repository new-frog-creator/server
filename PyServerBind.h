/*
 *  <one line to give the program's name and a brief idea of what it does.>
 *  Copyright (C) <year>  <name of author>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef PY_SERVER_BIND_H
#define PY_SERVER_BIND_H

#include <Python.h>
#include <boost/python.hpp>
#include "Server.h"
#include "User.h"
#include "Player.h"
#include "Utils.h"
#include "PlayerManager.h"
#include "XmlPlayerManager.h"

using namespace boost::python;


namespace Wrapper_ {
    class PlayerManagerWrapper : public PlayerManager, public wrapper<PlayerManager> {
    public:
        Player& ByID(unsigned long int id) {
            return this->get_override("ByID")(id);
        }
        Player& Create(const std::string& name) {
            return this->get_override("Create")(name);
        }
        void Save(Player& player) {
            this->get_override("Save")(player);
        }
        void Delete(Player& player) {
            this->get_override("Delete")(player);
        }
        bool HasID(unsigned long int id) {
            return this->get_override("HasID")(id);
        }
	bool HasName(const std::string& name) {
	    return this->get_override("HasName")(name);
	}
        Player& ByName(const std::string& name, const std::string& value) {
            return this->get_override("ByName")(name);
        }


    };

}

BOOST_PYTHON_MODULE(Sf)
{
    object PyIpAddress = class_<sf::IpAddress>("IpAddress",init<>())
    .def(init<std::string>())
    .def(init<char*>())
    .def(init<sf::Uint8,sf::Uint8,sf::Uint8,sf::Uint8>())
    .def(init<sf::Uint32>())
    .def("__str__", &sf::IpAddress::ToString)
    .def("ToInteger", &sf::IpAddress::ToInteger)
    ;

    object PyThread = class_<sf::Thread, boost::noncopyable>("Thread",no_init)
    .def("Launch",&sf::Thread::Launch)
    .def("Terminate",&sf::Thread::Terminate)
    ;
}



BOOST_PYTHON_MODULE(Frog)
{
    class_<Server, boost::noncopyable >("Server" ,no_init)
    .def("Running",&Server::IsRunning, return_value_policy<copy_const_reference>() )
    .def("LocalIp",&Server::GetLocalIP, return_value_policy<copy_const_reference>() )
    .def("InternetIp",&Server::GetInternetIP, return_value_policy<copy_const_reference>() )
    .def("SendTo",&Server::SendTo)
    .def("SendAll",&Server::SendAll)
    .def("Disconnect",&Server::Disconnect)
    .def_readonly("CountUsers",&Server::CountUsers)
    ;

    object PyUser = class_<User>("User",no_init)
    .def_readonly("ID",&User::GetID)
    .def_readonly("Logged",&User::IsLogged)
    .def("Login", &User::Login)
    .def("Logout",&User::Logout)
    .add_property("IP",&User::GetIP, &User::SetIP)
    ;

    object PyPlayerManager = class_<Wrapper_::PlayerManagerWrapper, boost::noncopyable>("PlayerManager",no_init)
    .def("ByID",pure_virtual(&PlayerManager::ByID), return_value_policy< reference_existing_object >())
    .def("Save",pure_virtual(&PlayerManager::Save))
    .def("Create",pure_virtual(&PlayerManager::Create), return_value_policy< reference_existing_object >())
    .def("Delete",pure_virtual(&PlayerManager::Delete))
    .def("HasID",pure_virtual(&PlayerManager::HasID))
    .def("HasName",pure_virtual(&PlayerManager::HasName))
    .def("ByName",pure_virtual(&PlayerManager::ByName), return_value_policy< reference_existing_object >())
    ;

    object PyPlayer = class_<Player>("Player",init<>())
    .add_property("ID",&Player::GetID,&Player::SetID)
    .def("Get",&Player::Get,return_value_policy< reference_existing_object >())
    .def("Set",&Player::Set)
    ;

    def("Parse",&Parse);

    object PyNode = class_<Node>("Node",init<>())
    .def("Get",&Node::Get,return_value_policy< reference_existing_object >() )
    .def("Set",&Node::Set)
    .def("Value",&Node::Value,return_value_policy< copy_const_reference >())
    //.def(str(self))
    ;

    object PyXmlPlayerManager = class_<XmlPlayerManager,bases<PlayerManager> > ("XmlPlayerManager",no_init);
}

#endif
