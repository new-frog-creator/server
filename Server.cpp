/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Server.h"

Server::Server(const sf::Uint16& listenPort)
{

    SetListenPort(listenPort);
    m_LocalIP = sf::IpAddress::GetLocalAddress();
    m_InternetIP = sf::IpAddress::GetPublicAddress();
    m_Running = false;
    m_NextUser = NULL;

    m_UserListener = new sf::TcpListener;
}

Server::~Server()
{
    
    std::map<sf::TcpSocket*,User*>::iterator it=m_Users.begin();
    while(it != m_Users.end()) {
        delete (*it).first;
        delete (*it).second;
        it++;
    }
    m_Users.clear();

    if(m_NextUser)
        delete m_NextUser;

    delete m_UserListener;
}

const sf::IpAddress& Server::GetLocalIP() const
{
    return m_LocalIP;
}

const sf::IpAddress& Server::GetInternetIP() const
{
    return m_InternetIP;
}

const sf::Uint16& Server::GetListenPort() const
{
    return m_ListenPort;
}

sf::Uint16 Server::CountUsers() const
{
    return m_Users.size();
}

void Server::SetListenPort(const sf::Uint16& listenPort)
{
    m_ListenPort = listenPort;
}

void Server::Start()
{
    if(m_UserListener->Listen(m_ListenPort) == sf::Socket::Done)
        m_Running = true;
    m_UserListener->SetBlocking(false);
}

void Server::Stop()
{
    m_Running = false;
    
}

const bool& Server::IsRunning()
{
    return m_Running;
}

void Server::Run()
{
    while(m_Running)
        Update();
}


void Server::Update()
{
    if(!m_Running) // Don't update anything if Server is not running
        return;


        if(m_NextUser == NULL)
            m_NextUser = new sf::TcpSocket;


        if(m_UserListener->Accept(*m_NextUser) == sf::Socket::Done) {
            m_Users[m_NextUser] = new User(m_NextUser,false,0);
            m_UserSelector.Add(*m_NextUser);
	    m_Users[m_NextUser]->SetIP(m_NextUser->GetRemoteAddress());
            m_Sig_Connection.emit(this,*m_Users[m_NextUser]);
            m_NextUser = new sf::TcpSocket;
        }


        m_UserSelector.Wait(0.1f); // Refresh all sockets

        std::map<sf::TcpSocket*,User*>::iterator it = m_Users.begin();
        while(it != m_Users.end()) {
            sf::TcpSocket& tmp = *((*it).first);
            if(m_UserSelector.IsReady(tmp)) {

                char buffer[2048];
                std::size_t size;

                sf::Socket::Status status = tmp.Receive(buffer, sizeof(buffer), size);

                if(status == sf::Socket::Done) {
//		    if(message[size-1]==(char)237)
//			message.erase(size-1);
		    std::string str = buffer;
                    str += '\0';
                    m_Sig_MessageReceived.emit(this,
                                               (*m_Users[&tmp])
                                               ,str);
                }
                else if(status == sf::Socket::Disconnected) {
                    m_Sig_Disjunction.emit(this,*m_Users[&tmp]);
                    m_UserSelector.Remove(tmp);
                    delete (*it).first;
                    delete (*it).second;
                    m_Users.erase(&tmp);
                    continue;
                }
            }
            it++;
        }
}

void Server::SendTo(User& user, std::string message)
{

    if(message[message.size()-1] != '\0') {
        message += '\0';
    }

    std::size_t size = message.size();
    std::cout << "Envoi de : " << message << std::endl;
    user.GetSocket()->Send(message.c_str(),size);
}

void Server::SendAll(const std::string& message)
{
    std::map<sf::TcpSocket*,User*>::iterator it = m_Users.begin();
    while(it != m_Users.end()) {
        SendTo(*(*it).second,message);
        it++;
    }
}

void
Server::Disconnect(User& user) {
    user.GetSocket()->Disconnect();
}


sigc::signal< void, Server*, User&>& Server::OnConnection()
{
    return m_Sig_Connection;
}

sigc::signal< void, Server*, User&>& Server::OnDisjunction()
{
    return m_Sig_Disjunction;
}

sigc::signal< void, Server*, User&, std::string>& Server::OnMessageReceived()
{
    return m_Sig_MessageReceived;
}
