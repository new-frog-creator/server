/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef SERVER_H
#define SERVER_H

#include <SFML/Network.hpp>
#include <sigc++/sigc++.h>
#include <string>
#include <iostream>

#include "User.h"
#include "Utils.h"

class Server : public sf::Thread
{
public:
    Server(const sf::Uint16& listenPort=2000);
    ~Server();

    const sf::IpAddress& GetLocalIP() const;
    const sf::IpAddress& GetInternetIP() const;
    const sf::Uint16& GetListenPort() const;

    sf::Uint16 CountUsers() const;

    void SetListenPort(const sf::Uint16& listenPort);

    void SendAll(const std::string& message);
    void SendTo(User& user, std::string message);
    void Disconnect(User& user);


    void Start();
    void Stop();
    void Update();

    const bool& IsRunning();

    sigc::signal<void, Server*, User&>& OnConnection();
    sigc::signal<void, Server*, User&>& OnDisjunction();
    sigc::signal<void, Server*, User&,std::string>& OnMessageReceived();

protected:


private:
    virtual void Run();

    bool m_Running;

    sf::IpAddress m_LocalIP;
    sf::IpAddress m_InternetIP;
    sf::Uint16 m_ListenPort;


    sf::TcpSocket* m_NextUser;
    sf::TcpListener* m_UserListener;
    sf::SocketSelector m_UserSelector;

    std::map<sf::TcpSocket*, User*> m_Users;

    sigc::signal<void, Server*, User&> m_Sig_Connection;
    sigc::signal<void, Server*, User&> m_Sig_Disjunction;
    sigc::signal<void, Server*, User&, std::string> m_Sig_MessageReceived;
};

#endif // SERVER_H
