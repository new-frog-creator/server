/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "Server.h"
#include "ServerActions.h"


using namespace boost::python;

ServerActions::ServerActions() {
    m_MainModule = import("__main__");
    m_Globals = m_MainModule.attr("__dict__");

}

void ServerActions::Connexion(Server* srv, User& user) {
    std::cout << "Connexion d'un utilisateur : " << user.GetID() << std::endl;
}
void ServerActions::Disjunction(Server* srv, User& user) {
    std::cout << "Deconnexion d'un utilisateur : " << user.GetID() << std::endl;
}
void ServerActions::Message(Server* srv, User& user, std::string message) {
    std::string action = Parse(message,0,(char)23);
    std::string filename = "./modules/client/"+action+".py";
    if(!FileExists(filename)){
        std::cout << "action FIXME (user " << user.GetID() <<") : " << action << " ==> "  << message <<std::endl;
        return;

    }

    std::cout <<"User " << user.GetID() << " ===> " << message << std::endl;

    try{
        m_Globals["message"]= message.c_str();
        m_Globals["sender"] = object(ptr(&user));
        exec_file(filename.c_str(), m_Globals);
    }
    catch(error_already_set& e){
        PyErr_Print();
        std::cout << "Erreur : already set"  << std::endl;
    }

}
void ServerActions::Admin(Server* srv, User& admin, std::string message) {

    std::cout << "Message reçu : " << message << std::endl;
    if(message == "/fermer")
        exit(0);
    else if(message == "/total") {
        std::string response = "Il y a " + Converter::ToString(srv->CountUsers()) + " client(s) connecté(s)" + '\0';
        srv->SendTo(admin,response);
    }
}

object& ServerActions::GetGlobals() {

    return m_Globals;
}
