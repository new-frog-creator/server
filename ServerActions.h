/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef SERVERACTIONS_H
#define SERVERACTIONS_H

#include <Python.h>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/python.hpp>
#include "Converter.h"
#include "Utils.h"
#include "User.h"

class Server;

class ServerActions
{
public:
    ServerActions();
    void Admin(Server* srv, User& admin, std::string message);
    void Message(Server* srv, User& user, std::string message);
    void Disjunction(Server* srv, User& user);
    void Connexion(Server* srv, User& user);

    boost::python::object& GetGlobals();
private:
    boost::python::object m_MainModule,
                        m_Globals;
};

#endif // SERVERACTIONS_H
