/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "User.h"

User::User(sf::TcpSocket* socket, bool isLogged, long unsigned int id)
{
    m_Socket = socket;
    m_IsLogged = isLogged;
    m_ID = id;
}

long unsigned int User::GetID()
{
    return m_ID;
}

sf::TcpSocket* User::GetSocket()
{
    return m_Socket;
}

bool User::IsLogged()
{
    return m_IsLogged;
}

void User::Login()
{
    m_IsLogged = true;
}

void 
User::Logout() {
    m_IsLogged = false;
}

void 
User::SetIP(const sf::IpAddress& ip) {
    m_IP = ip;
}

sf::IpAddress 
User::GetIP() const {
    return m_IP;
}


bool User::operator==(const User& other)
{
    return m_Socket == other.m_Socket;
}
