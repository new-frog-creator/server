/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef USER_H
#define USER_H
#include <SFML/Network.hpp>

class User
{
public:
    User(sf::TcpSocket*,bool isLogged=false,unsigned long int id=0);

    sf::TcpSocket* GetSocket();
    unsigned long int GetID();
    bool IsLogged();

    sf::IpAddress GetIP() const;
    void SetIP(const sf::IpAddress& ip);
    void Login();
    void Logout();

    bool operator==(const User& other);

private:
    sf::TcpSocket* m_Socket;
    unsigned long int m_ID;
    sf::IpAddress m_IP;
    bool m_IsLogged;

};

#endif // USER_H
