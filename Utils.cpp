#include "Utils.h"

std::string Parse (const std::string& str,const unsigned int& part, const char& delim) {
    std::string parsed;
    std::istringstream iss(str);
    unsigned int i = 0;
    while(i<= part && std::getline(iss, parsed, delim)) {
        i++;
    }
    if(i<= part)
        return "";
    return parsed;
}

bool FileExists(const std::string& filename)
{
    std::ifstream file(filename.c_str());
    if(file)
        return true;
    return false;
}

std::string Replace(const char* str, char element, char replace,std::size_t size)
{
    unsigned int i = 0;
    std::string tmp;
    while(i<size) {
        if(str[i] == element)
            tmp+=replace;
        else
            tmp += str[i];
        i++;
    }
    return tmp.c_str();
}
