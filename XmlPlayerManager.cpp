/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "XmlPlayerManager.h"
#include "Player.h"

XmlPlayerManager::XmlPlayerManager(const std::string& datapath) :
    PlayerManager() {

    xmlKeepBlanksDefault(0);
    SetDataPath(datapath);

    std::string filename = m_DataPath+"Accounts.xml"; 

    xmlDocPtr doc;
    xmlNodePtr root;

    doc = xmlParseFile(filename.c_str());
    if(doc) {
	root = xmlDocGetRootElement(doc);
	if(root && root->name == (xmlChar*)"playerslist") {
	    xmlNodePtr n;
	    for(n=root; n!=NULL; n = n->next) {
		if(n->type == XML_ELEMENT_NODE && n->name == (xmlChar*)"player") {
		    xmlNodePtr subn;
		    std::string pseudo;
		    unsigned long int id;
		    xmlChar* psd = xmlGetProp(n,(xmlChar*)"pseudo");
			pseudo = (const char*)psd;
			xmlFree(psd);
		    xmlChar* sid  = xmlGetProp(n,(xmlChar*)"id");
			id = Converter::FromString<unsigned long int>((const char*)sid);
			xmlFree(psd);
		    m_PlayersByName[pseudo] = id;
		}
	    }
	}
    }
}

XmlPlayerManager::~XmlPlayerManager() {
    xmlDocPtr doc;
    xmlNodePtr root;

    if((doc = xmlNewDoc((xmlChar*)"1.0"))==NULL)
	return; // TODO: launch an exception and catch it after

    if((root = xmlNewNode(NULL,(xmlChar*)"playerslist"))==NULL) 
	return; // TODO: launch an exception and catch it after

    std::map<unsigned long int, Player*>::iterator it = m_Players.begin();
    while(it != m_Players.end()) {
	xmlNodePtr node;
	if((node = xmlNewNode(NULL,(xmlChar*)"player"))==NULL)
	    return; // TODO: launch an exception
	
	xmlSetProp(node, (xmlChar*)"pseudo", (xmlChar*)(((*it).second)->GetName().c_str())); // player.GetName 
	xmlSetProp(node, (xmlChar*)"id",(xmlChar*)Converter::ToString((*it).first).c_str()); // player.GetID

	xmlAddChild(root,node);
	
	it++;
    }

    xmlDocSetRootElement(doc, root);

    xmlSaveFile((m_DataPath+"Accounts.xml").c_str(),doc);
}

void 
XmlPlayerManager::SetDataPath(std::string path) {
    if(boost::filesystem::is_directory(path))
        m_DataPath = path;
    if(m_DataPath == "")
	m_DataPath = "./";
    if(m_DataPath[m_DataPath.size()-1] != '/')
	m_DataPath+= '/';
}

Player&
XmlPlayerManager::Create(const std::string& name) {
    unsigned long int i = 0;
    bool idFounded;
    std::string filename;
    while(!idFounded) {
        filename = m_DataPath+Converter::ToString(i)+".xml";
        if(!boost::filesystem::exists(filename) && !boost::filesystem::is_directory(filename))
            idFounded = true;
        i++;
    }
    xmlDocPtr doc = xmlNewDoc((xmlChar*)"1.0");
    if(!doc)
	throw std::runtime_error("Can't make player document");

    xmlNodePtr root = xmlNewNode(NULL,(xmlChar*)"player");
    if(!root)
	throw std::runtime_error("Can't make player document root");
    xmlDocSetRootElement(doc, root);
    xmlSaveFile(filename.c_str(),doc);

    m_PlayersByName[name]=i;
    m_Players[i]=new Player();
    m_Players.at(i)->SetName(name);
    m_Players.at(i)->SetID(i-1);
    return *m_Players.at(i);
}

void 
XmlPlayerManager::Delete(Player& player) {
    unsigned long int id = player.GetID();
    Player* tmp = GetCached( id);
    if( tmp != NULL) {
        delete tmp;
        tmp = NULL;
    }
    boost::filesystem::remove(m_DataPath+Converter::ToString(id)+".xml");
}



Player& 
XmlPlayerManager::ByName(const std::string& name) {
    return ByID(m_PlayersByName.at(name));
}

Player& 
XmlPlayerManager::ByID(long unsigned int id) {
    // TODO: raise an exception
    Player* tmp = GetCached(id);
    if(tmp == NULL) {
	// TODO: raise an exception if player doesn't exist
        tmp = new Player;
	tmp->SetID(id);
        m_Players[id] = tmp;
	xmlDocPtr doc;
	xmlNodePtr root;

	std::string filename = m_DataPath+Converter::ToString(id)+".xml";
	doc = xmlParseFile(filename.c_str());
	if(!doc) {
	    throw std::runtime_error("Unknown file " + filename);	
	}
	root = xmlDocGetRootElement(doc);
	if(!root) {
	    throw std::runtime_error("Can't make root node");
	}
	
	if(root->type == XML_ELEMENT_NODE && root->children != NULL) {
	    xmlNodePtr n;
	    for(n = root; n != NULL; n = n->next) {
		Node& node = tmp->Get((const char*)n->name); 
		GetImpl(node, n);
	    }
	}

    }
    return *tmp;
}

void
XmlPlayerManager::GetImpl(Node& node, xmlNodePtr ptr) {
    
    if(ptr->children == NULL)
	return;
    xmlNodePtr n;
    for(n=ptr;n != NULL; n = n->next) {
	if(n->type == XML_ELEMENT_NODE) {
	    if(n->name == (xmlChar*)"value") {
		xmlChar* content = xmlNodeGetContent(n);
		node = (const char*)content;
		xmlFree(content);
	    }
	    else {
		GetImpl(node.Get((const char*)n->name), n);
	    }
	}
    }
}

bool 
XmlPlayerManager::HasID(long unsigned int id) {
    if(boost::filesystem::exists( m_DataPath+Converter::ToString(id)+".xml"))
        return true;
    return false;
}
bool 
XmlPlayerManager::HasName(const std::string& name) {
    return m_PlayersByName.find(name) != m_PlayersByName.end();
}

void 
XmlPlayerManager::Save(Player& player) {
    xmlDocPtr doc;
    xmlNodePtr root;

    if((doc = xmlNewDoc((xmlChar*)"1.0"))==NULL)
	return; // TODO: launch an exception and catch it after ( a python exception ?)

    if((root = xmlNewNode(NULL,(xmlChar*)"player"))==NULL)
	return; // TODO: launch an exception and catch it after ( a python exception ?)

    xmlDocSetRootElement(doc,root);

    const std::map<std::string,Node*>& pkeys =  player.GetKeys(); // TODO: implements visitor
    std::map<std::string,Node*>::const_iterator key;
    key = pkeys.begin();
    while(key != pkeys.end()) {
	SaveImpl(root,(*key).first,*(*key).second);
	key++;
    }
    std::string filename = m_DataPath+Converter::ToString(player.GetID())+".xml";
    xmlSaveFile(filename.c_str(),doc);
    
}

void 
XmlPlayerManager::SaveImpl(xmlNodePtr parent, const std::string& nodename, const Node& element) {

    xmlNodePtr node,value;
    if((node = xmlNewNode(NULL,(xmlChar*)nodename.c_str()))== NULL)
	return; // TODO: launch an exception and catch it after ( a python exception ?)

    if(element.Value() != "") {
	if((value = xmlNewNode(NULL,(xmlChar*)"value"))==NULL)
	    return; // TODO: launch an exception and catch it after ( a python exception ?)
	
	xmlNodeSetContent(value,(xmlChar*)element.Value().c_str());
	xmlAddChild(node,value);
    }
    xmlAddChild(parent,node);

    const std::map<std::string,Node*>& keys = element.GetKeys();
    std::map<std::string,Node*>::const_iterator key = keys.begin();
    while(key != keys.end()) {
	SaveImpl(node, (*key).first,*(*key).second);
	key++;
    }
}

Player* 
XmlPlayerManager::GetCached(long unsigned int id) {
    try{
        return m_Players.at(id);
    }
    catch(...){
        return NULL;
    }
}
