/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef XMLPLAYERMANAGER_H
#define XMLPLAYERMANAGER_H

#include "PlayerManager.h"
#include "Converter.h"
#include <exception>
#include <boost/filesystem.hpp>
#include <map>
#include <fstream>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include "Node.h"
#include "Player.h"

class XmlPlayerManager : public PlayerManager
{
public:
    XmlPlayerManager(const std::string& datapath="");
    ~XmlPlayerManager();

    void SetDataPath(std::string path);

    Player& ByID(unsigned long int id);
    Player& ByName(const std::string& name);
    Player& Create(const std::string& name);
    void Save(Player& player);
    void Delete(Player& player);
    bool HasID(unsigned long int id);
    bool HasName(const std::string& name);


protected:
    Player* GetCached(unsigned long int id);
    void    SaveImpl(xmlNodePtr parent, const std::string& nodename, const Node& element);
    void    GetImpl(Node&, xmlNode*);
private:
    std::string m_DataPath;
    std::map<std::string, unsigned long int> m_PlayersByName;
    std::map<unsigned long int, Player*> m_Players;
};

#endif // XMLPLAYERMANAGER_H
