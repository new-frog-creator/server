#include <iostream>
#include <fstream>
#include <sstream>

#include <Python.h>
#include <boost/python.hpp>
#include <sigc++/sigc++.h>
#include <SFML/Network.hpp>
#include "Server.h"
#include "ServerActions.h"
#include "Converter.h"
#include "Utils.h"
#include "PyServerBind.h"
#include "XmlPlayerManager.h"

using namespace boost::python;



int main(int argc, char **argv) {

    std::string root = boost::filesystem::current_path().string();

    PyImport_AppendInittab( "Frog", &initFrog );
    PyImport_AppendInittab( "Sf", &initSf );
    Py_Initialize();

    ServerActions serveraction;

    object frogMod( (handle<>(PyImport_ImportModule("Frog"))) );
    object sfMod( (handle<>(PyImport_ImportModule("Sf"))) );
    try {
    serveraction.GetGlobals()["Frog"] = frogMod;
    serveraction.GetGlobals()["Sf"] = sfMod;
}
catch(error_already_set& e) {
    PyErr_Print();
}
    std::cout << "Start Server" << std::endl
                << "\tPort Admin : 5005" << std::endl
                << "\tPort Player: 9854" << std::endl;
    Server server(9854);

        server.OnConnection().connect(sigc::mem_fun(serveraction,&ServerActions::Connexion));
        server.OnDisjunction().connect(sigc::mem_fun(serveraction,&ServerActions::Disjunction));
        server.OnMessageReceived().connect(sigc::mem_fun(serveraction,&ServerActions::Message));

    Server admin(5005);
        admin.OnMessageReceived().connect(sigc::mem_fun(serveraction,&ServerActions::Admin));

    XmlPlayerManager playerManager("./datas/players/");

try {
    
    scope(frogMod).attr("Game") = object(ptr(&server));
    scope(frogMod).attr("Admin") = object(ptr(&admin));
    scope(frogMod).attr("Players") = object(ptr(&playerManager));
    std::cout << "Ajout du dossier " << root << "/modules/framework/" << std::endl;
    std::string pycode = "import sys\nsys.path.append('"+root+"/modules/framework/')";
    exec(pycode.c_str(),serveraction.GetGlobals(), serveraction.GetGlobals());
}
catch(error_already_set& e) {
    PyErr_Print();
}

        server.Start();
        server.Launch();

        admin.Start();
        admin.Launch();
/*while(server.IsRunning() && admin.IsRunning()) {
    server.Update();
    admin.Update();
}*/

    
    
    std::cout << "Commandline, enter help for a list of commands : " << std::endl;
    while(server.IsRunning() && admin.IsRunning()) {
        std::cout << " >" << std::flush;
        std::string command;
        std::getline(std::cin,command);

        std::string action = Parse(command,0,'\n');
        if(action == "quit") {
            server.Stop();
            admin.Stop();
        }

        std::cin.clear();
    }

    return 0;
}
