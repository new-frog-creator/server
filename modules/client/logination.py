# LOGINATION	:   login player into game
# INPUT SYNTAX	:   logination SEP name SEP password SEP version.MAJOR SEP version.MINOR SEP version.REVISION SEP securityKey1 SEP securityKey2 SEP securityKey3 SEP securityKey4 SEP END
# OUTPUT SYNTAX :   Message.MaxInfo()
# AUTHOR	:   Alexandre Janniaux

import Conf
import Auth
from Message import *

action= name= password= major_version= minor_version= revision= key1= key2= key3= key4 = ''
try:
    action, name, password, major_version, minor_version, revision, key1, key2, key3, key4, end = message.split(Conf.SEP)
except ValueError as a:
    print ('erreur de valeur')

if sender.Logged:
    Frog.Game.SendTo(sender,PlainMsg('Vous etes deja connecte !',1))

else:
    if not Auth.IsNameValid(name):
	Frog.Game.SendTo( sender, PlainMsg(Conf.PLAYER_CHARSET_ERROR,1) )

    elif int(major_version) < Conf.MAJOR_VERSION or int(major_version) > Conf.MAJOR_VERSION:
	Frog.Game.SendTo(sender,PlainMsg('Votre client n\'est pas compatible',1))

    elif int(minor_version) < Conf.MINOR_VERSION:
	Frog.Game.SendTo(sender,PlainMsg('Votre client n\'est pas a jour',1))

    elif int(minor_version) > Conf.MINOR_VERSION:
	Frog.Game.SendTo(sender,PlainMsg('Le serveur a subit une retrogradation',1))

    else:
	if not Frog.Players.HasName(name):
	    Frog.Game.SendTo(sender,PlainMsg('Mauvais mot de passe ou nom de compte',1))
	else:
	    player = Frog.Players.ByName(name)
	    print ('MDP ENVOYE : "%s" && MDP STOCKE : "%s"' % (password, player.Get('password').Value()))
	    if not Auth.IsPasswordForAccount(player, password):
		Frog.Game.SendTo(sender,PlainMsg('Mauvais mot de passe ou nom de compte',1))
	    else:
		sender.Login()
		Frog.Game.SendTo(sender,MaxInfo())
		print ('%s s\'est connecte avec l\'ip %s !' % (name,sender.IP))
