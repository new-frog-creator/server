# -*- coding: utf-8 -*
# NEWFACCOUNTIED :      Create account
# INPUT SYNTAX :        newfaccountied SEP name SEP password
# OUTPUT SYNTAX:
# AUTHOR :              alexandre janniaux
import Message
import Conf

sep = Conf.SEP
end = Conf.END
name = Frog.Parse(message,1,sep)
password = Frog.Parse(message,2,sep)
if Frog.Players.HasName(name):
    print ('Pseudo deja existant')
    Frog.Game.SendTo (sender,Message.PlainMsg('Pseudo deja existant',1))

else:
    player = Frog.Players.Create(name)
    player.Set("pseudo", name)
    player.Set("password", password)
    print (player.ID)
    Frog.Players.Save(player)
    Frog.Game.SendTo (sender, Message.PlainMsg('Votre compte a ete cree',1))
    
