# SERVERRESULTS :       Send server information to the client
# INPUT SYNTAX :        serverresults
# RETURN SYNTAX :       serverresults SEP id SEP totalNumberOfClients SEP maxClients SEP END
# AUTHOR :              alexandre janniaux
import Conf
sep = Conf.SEP
end = Conf.END
serverresults = 'serverresults' + sep + str(0) + sep + str(Frog.Game.CountUsers) + sep + str(Conf.MAX_PLAYERS) + sep + end
Frog.Game.SendTo(sender, serverresults)
