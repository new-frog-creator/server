# AUTH	    :	User fonctions
# AUTHOR    :	Alexandre Janniaux

import Conf

def IsNameValid(name):
    charset = tuple(Conf.PLAYER_CHARSET)
    t = tuple(name)
    for k in t:
	equality = False
	for c in charset:
	    if c==k:
		equality = True
		break

	if equality == False:
	    return False
    return True

def IsPasswordForAccount(player,password):
    return player.Get("password").Value() == password
