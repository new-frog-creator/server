
PLAYER_CHARSET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
PLAYER_CHARSET_ERROR = 'Le nom ne doit contenir que des lettres et des chiffres'

SEP = chr(23)
END = chr(0)

GAME_NAME = 'test'

MAJOR_VERSION = 0
MINOR_VERSION = 5 


MAX_PLAYERS	= 50
MAX_ITEMS	= 100
MAX_NPCS	= 100
MAX_SHOP	= 100
MAX_SPELLS	= 100
MAX_MAPS	= 100
MAX_MAP_ITEMS	= 100
MAX_MAPX	= 100
MAX_MAPY	= 100
MAX_EMOTICONS	= 100
MAX_LEVEL	= 100
MAX_QUETES	= 100
MAX_INV		= 100
MAX_NPC_SPELLS	= 100
