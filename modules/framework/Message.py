import Conf
from Conf import SEP, END

def PlainMsg(message,num):
    return 'PLAINMSG' + SEP + message + SEP + str(num) + SEP + END

def GlobalMsg(message,color):
    return 'GLOBALMSG' + SEP + message + SEP + str(color) + SEP + END


def MaxInfo():
    message = 'MAXINFO' + SEP  
    message += str(Conf.GAME_NAME)	+ SEP  
    message += str(Conf.MAX_PLAYERS)	+ SEP  
    message += str(Conf.MAX_ITEMS)	+ SEP  
    message += str(Conf.MAX_NPCS)	+ SEP  
    message += str(Conf.MAX_SHOP)	+ SEP 
    message += str(Conf.MAX_SPELLS)	+ SEP 
    message += str(Conf.MAX_MAPS)	+ SEP
    message += str(Conf.MAX_MAP_ITEMS)	+ SEP
    message += str(Conf.MAX_MAPX)	+ SEP
    message += str(Conf.MAX_MAPY)	+ SEP
    message += str(Conf.MAX_EMOTICONS)	+ SEP
    message += str(Conf.MAX_LEVEL)	+ SEP
    message += str(Conf.MAX_QUETES)	+ SEP
    message += str(Conf.MAX_INV)	+ SEP
    message += str(Conf.MAX_NPC_SPELLS) + SEP
    message += END
    return message 
